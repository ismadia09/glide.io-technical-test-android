package com.example.glideiotechnicaltest.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.glideiotechnicaltest.data.api.CarService
import com.example.glideiotechnicaltest.model.Car
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class CarListViewModel : ViewModel() {

    val cars = MutableLiveData<List<Car>>()

    private val remoteCars = MutableLiveData<List<Car>>()

    private val carService: CarService = CarService()
    private val disposable: CompositeDisposable = CompositeDisposable()


    fun refresh() {
        fetchDataFromRemoteApi()
    }


    fun fetchDataFromRemoteApi() {
        disposable.add(
            carService.getCars()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Car>>() {
                    override fun onSuccess(carList: List<Car>) {
                        cars.value = carList
                        remoteCars.value = carList
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                    }
                })
        )
    }

    fun searchCar(query: String) {
        val filteredCars = remoteCars.value?.filterIndexed { index, car ->
            car.name.contains(query, true) || car.plate.contains(query, true)
        }
        if (filteredCars != null) {
            cars.value = filteredCars!!
        }
    }


    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}