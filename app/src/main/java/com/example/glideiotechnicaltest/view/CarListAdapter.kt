package com.example.glideiotechnicaltest.view

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.glideiotechnicaltest.R
import com.example.glideiotechnicaltest.model.Car
import com.google.gson.Gson

class CarListAdapter(
    private val carList: ArrayList<Car>,
) : RecyclerView.Adapter<CarViewHolder>() {

    fun updateCarList(updatedCars: List<Car>) {
        carList.clear()
        carList.addAll(updatedCars)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.car_list_item, parent, false)
        return CarViewHolder(view)
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        val car = carList[position]
        holder.titleView.text = car.name
        holder.licencePlateView.text = car.plate
        holder.statusView.text = car.status
        holder.statusView.setBackgroundColor(car.getStatusColor());

        Glide.with(holder.imageView)
            .load(car.imageUrl)
            .placeholder(R.drawable.missing_image_placeholder)
            .error(R.drawable.missing_image_placeholder)
            .into(holder.imageView)

        holder.itemView.setOnClickListener {
            val context = holder.itemView.context
            val intent = Intent(context, CarDetailActivity::class.java)
            val json = Gson().toJson(car)
            intent.putExtra("car", json)
            context.startActivity(intent)

        }


    }

    override fun getItemCount(): Int {
        return carList.size
    }
}


class CarViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val titleView: TextView = view.findViewById(R.id.car_list_item_title)
    val licencePlateView: TextView = view.findViewById(R.id.car_list_item_plate)
    val imageView: ImageView = view.findViewById(R.id.car_list_item_iv)
    val statusView: TextView = view.findViewById(R.id.car_list_item_status)


}