package com.example.glideiotechnicaltest.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.glideiotechnicaltest.R
import com.example.glideiotechnicaltest.databinding.ActivityCarDetailBinding
import com.example.glideiotechnicaltest.model.Car
import com.google.gson.Gson

class CarDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCarDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCarDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val json = intent.getStringExtra("car")
        val car = Gson().fromJson(json, Car::class.java)
        supportActionBar?.title = car.name
        binding.carDetailCarName.text = car.name
        binding.carDetailLicencePlate.text = car.plate
        binding.carDetailStatus.text = car.status
        binding.carDetailStatus.setBackgroundColor(car.getStatusColor());

        Glide.with(binding.carDetailImage)
            .load(car.imageUrl)
            .placeholder(R.drawable.missing_image_placeholder)
            .error(R.drawable.missing_image_placeholder)
            .into(binding.carDetailImage)
    }
}