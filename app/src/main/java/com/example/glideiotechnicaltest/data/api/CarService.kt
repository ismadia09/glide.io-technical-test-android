package com.example.glideiotechnicaltest.data.api

import com.example.glideiotechnicaltest.model.Car
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class CarService {

    private val BASE_URL = "https://api.jsonbin.io/v3/"

    private val api = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(CarAPI::class.java)


    fun getCars(): Single<List<Car>> {
        return api.getCars();
    }


}