package com.example.glideiotechnicaltest.data.api

import com.example.glideiotechnicaltest.model.Car
import io.reactivex.Single
import retrofit2.http.GET

interface CarAPI {

    @GET("b/6380b9b365b57a31e6c3a713?meta=false")
    fun getCars(): Single<List<Car>>
}