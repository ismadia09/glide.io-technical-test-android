package com.example.glideiotechnicaltest

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.glideiotechnicaltest.databinding.ActivityMainBinding
import com.example.glideiotechnicaltest.view.CarListAdapter
import com.example.glideiotechnicaltest.viewmodel.CarListViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: CarListViewModel

    private val carListAdapter = CarListAdapter(arrayListOf())


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        viewModel = ViewModelProvider(this).get(CarListViewModel::class.java)
        viewModel.refresh()

        binding.carListRv.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = carListAdapter
        }

        binding.carListSearchView.clearFocus()
        binding.carListSearchView.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                viewModel.searchCar(newText)
                return false
            }
        })


        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.cars.observe(this, Observer { carList ->
            carList.let {
                carListAdapter.updateCarList(carList)
            }

        })
    }
}

