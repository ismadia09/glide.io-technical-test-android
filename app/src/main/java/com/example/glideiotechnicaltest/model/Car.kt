package com.example.glideiotechnicaltest.model


import android.graphics.Color
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Car(
    @SerializedName("id")
    @Expose
    val id: String,
    @SerializedName("imageUrl")
    @Expose
    val imageUrl: String,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("plate")
    @Expose
    val plate: String,
    @SerializedName("status")
    @Expose
    val status: String
) {
    fun getStatusColor(): Int {
        when (status) {
            "READY" -> return Color.parseColor("#FF008000")
            "INACTIVE" -> return Color.parseColor("#FFFF0000")
            "MAINTENANCE" -> return Color.parseColor("#FFFFD700")
            else -> {
                return Color.parseColor("#FFFFFFFF")
            }
        }
    }
}