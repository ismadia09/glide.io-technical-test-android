# Glide.io Technical Test

## Présentation

* Le projet a été compilé avec le SDK 33, avec minSDK 21
* Langage utilisé : Kotlin
* Architecture : MVVM (Model, View, ViewModel)

## Librairies utilisées

* Retrofit : Permet les appels réseaux à une API à distante et la sérialisation des données reçues.
* Glide pour les images

## Evolutions possibles

* Effectuer des tests unitaires
* Sauvegarder les données localement


